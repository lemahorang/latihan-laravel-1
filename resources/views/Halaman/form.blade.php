@extends('layout.master')

@section('judul')
Buat Account Baru
@endsection

@section('content')
    <h1>Buat Account Baru</h1>
    <h3>Sign Up Form</h3>
    <form action="/welcome" method="post">
    @csrf
    <label>First Name :</label><br>
    <input type="text" name="nama"><br><br>
    <label>Last Name :</label><br>
    <input type="text" name="nama"><br><br>
    <label>Gender</label><br>
    <input type="radio" name="select">Male<br>
    <input type="radio" name="select">Female<br>
    <input type="radio" name="select">Other<br><br>
    <label>Nationality :</label><br>
    <select addres="Indonesia"><br>
        <option value="17">Indonesia</option>
        <option value="18">Malaysia</option>
        <option value="19">Thailand</option>
    </select><br><br>
    <label>Language Spoken</label><br>
    <input type="checkbox" name="skill">Bahasa Indonesia<br>
    <input type="checkbox" name="skill">English<br>
    <input type="checkbox" name="skill">Other<br><br>
    <label>Alamat Lengkap</label><br>
    <textarea name="address" cols="30" rows="5"></textarea><br>
    <input type="submit">
    </form>
@endsection