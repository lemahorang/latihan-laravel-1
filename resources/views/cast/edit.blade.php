@extends('layout.master');
@section('name')
    laman edit
@endsection

@section('content')
@section('content')
<form method="POST" action="/cast">
    @csrf
    <div class="form-group">
      <label for="exampleInputnama1">Nama</label>
      <input type="text" name="nama" value="{{$cast->nama}}" class="form-control" >
    </div>
    @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label for="exampleInputumur1">Umur</label>
      <input type="text" name="umur"  value="{{$cast->umur}}" class="form-control" >
    </div>
    @error('umur')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label for="exampleInputbio1">Bio</label>
        <textarea name="bio" cols="20" rows="10" class="form-control"> {{$cast->bio}}</textarea>
      </div>
      @error('deskripsi')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>

@endsection