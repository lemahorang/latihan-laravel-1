@extends('layout.master');
@section('name')
    laman tambah kategori
@endsection

@section('content')

<a href="/cast/create" class="btn btn-primary mb-3">Tambah Cast</a>

<table class="table">
    <thead class="thead-light">
      <tr>
        <th scope="col">#</th>
        <th scope="col">Nama</th>
        <th scope="col">Umur</th>
        <th scope="col">Bio</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($cast as $key => $item)
            <tr>
                <td>{{$key + 1}}</td>
                <td>{{$item->nama}}</td>
                <td>{{$item->umur}}</td>
                <td>{{$item->bio}}</td>
                <td>
                    <a href="/cast/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
                    <a href="/cast/{{$item->id}}/edit" class="btn btn-warning btn-sm">edit</a>

                    <form class="mt-2" action="/cast/{{$item->id}}" method="POST">
                        @csrf
                        @method('delete')
                        <input type="submit" value="delete" class="btn btn-danger btn-sm">
                    </form>
                </td>
            </tr>
        @empty
            
        @endforelse
    </tbody>
@endsection