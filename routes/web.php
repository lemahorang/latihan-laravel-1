<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
route::get('/', 'DasbordController@index');

Route::get('/form', 'formController@bio');

Route::post('/welcome', 'formController@submit');

Route::get('/datatable', function(){
    return view('table.d-tabel'); 
});


//CRUD

Route::get('/cast/create', 'castController@create');
Route::post('/cast', 'castController@store');

//read
Route::get('/cast', 'castController@index');
Route::get('/cast/{cast_id}', 'castController@show');

//update
Route::get('/cast/{cast_id}/edit', 'castController@edit');
Route::put('/cast/{cast_id}', 'castController@update');

//delete
Route::delete('/cast/{cast_id}', 'castController@destroy');
