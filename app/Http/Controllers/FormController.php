<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FormController extends Controller
{
    public function bio(){
        return view('halaman.form');
    }

    public function submit(request $request){
        $nama = $request ['nama'];
        $alamat = $request ['alamat'];
        return view('halaman.home', compact('nama','alamat'));
    }
}
